const apiURL = 'https://web-api-staging-ys.herokuapp.com/readers/'
var reader = {
  username: 'testName',
  password: 'testPsd'
}

describe('Login page', () => {
  beforeEach(() => {
    cy.visit('/#/login')
  })

  it('Shows a header', () => {
    cy.get('.vue-title')
      .should('contain', 'Login')
  })

  before(() => {
    cy.request('POST', apiURL, reader)
  })

  describe('With valid attributes', () => {
    it('Allows user login', () => {
      cy.get('#username_lbl')
        .contains('Username')
        .next()
        .type('testName')
      cy.get('#psd_lbl')
        .contains('Password')
        .next()
        .type('testPsd')
      cy.contains('is Required')
        .should('not.exist')
      cy.get('.typo__p')
        .should('not.exist')
      cy.get('#btn')
        .contains('Login')
        .click()
      cy.get('.typo__p')
        .should('contain', 'Welcome testName!')
    })
  })

  describe('With invalid/blank attributes', () => {
    it('Shows error messages', () => {
      cy.get('.error')
        .should('contain', 'is Required')
      cy.get('#username_input')
        .type('testName')
      cy.get('#psd_input')
        .type('wrongPsd')
      cy.get('#btn')
        .click()
      cy.get('.typo__p')
        .should('contain', 'Wrong Password')
    })
  })

  after(() => {
    var id = ''
    cy.wait(1000)
      .request('GET', `${apiURL}testName`)
      .its('body')
      .then(reader => {
        id = reader._id
        console.log(id)
        cy.request('DELETE', `${apiURL}${id}`)
      })
  })
})
