const apiURL = 'https://web-api-staging-ys.herokuapp.com/books/'

describe('Pages about books', () => {
  before(() => {
    cy.request(apiURL)
      .its('body')
      .then(books => {
        books.forEach(e => {
          cy.request('DELETE', `${apiURL}${e._id}`)
        })
      })
    cy.fixture('books').then(books => {
      let [b1, b2, b3] = books
      let three = [b1, b2, b3]
      three.forEach(b => {
        cy.request('POST', apiURL, b)
      })
    })
  })

  describe('Add new book page', () => {
    beforeEach(() => {
      cy.visit('/#/addBook')
    })

    it('Shows a header', () => {
      cy.get('.vue-title')
        .should('contain', 'Add a New Book')
    })

    describe('With valid attributes', () => {
      it('Adds a new book', () => {
        cy.get('.form__label')
          .contains('Book Title')
          .next()
          .type('Harry Potter and the Order of the Phoenix')
        cy.get('.form__label')
          .contains('Author')
          .next()
          .type('J.K. Rowling')
        cy.get('.form__label')
          .contains('ISBN')
          .next()
          .type('0439358078')
        cy.get('.form__label')
          .contains('Publishing House')
          .next()
          .type('Scholastic Inc')
        cy.get('.form__label')
          .contains('Publication Date')
          .next()
          .type('Sep 1 2004')
        cy.get('.form__label')
          .contains('Number of Pages')
          .next()
          .type(870)
        cy.get('#addBtn')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Added successfully!')
      })

      after(() => {
        cy.visit('/#/books')
        cy.wait(500)
        cy.get('tbody')
          .find('tr')
          .should('have.length', 4)
      })
    })

    describe('With invalid/blank attributes', () => {
      it('Shows error messages', () => {
        cy.get('.error')
          .should('contain', 'is Required')
        cy.get('#numOfPages_input')
          .type('wrong')
        cy.get('.error')
          .should('contain', 'Numeric')
        cy.get('#addBtn')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Please Fill in the Form Correctly.')
      })
    })
  })

  describe('Books page', () => {
    beforeEach(() => {
      cy.visit('/#/books')
    })

    it('Shows a header', () => {
      cy.get('.vue-title')
        .should('contain', 'Book List')
    })

    describe('For a cancelled delete operation', () => {
      it('Leaves the list unchanged', () => {
        cy.wait(500)
        cy.get('tbody')
          .find('tr')
          .eq(3)
          .find('td')
          .eq(8)
          .find('a')
          .click()
        cy.get('button')
          .contains('Cancel')
          .click()
        cy.get('tbody')
          .find('tr')
          .should('have.length', 4)
      })
    })

    describe('For a confirmed delete operation', () => {
      it('Reduces the no. of books by 1', () => {
        cy.wait(500)
        cy.get('tbody')
          .find('tr')
          .eq(3)
          .find('td')
          .eq(8)
          .find('a')
          .click()
        cy.get('button')
          .contains('Delete')
          .click()
        cy.get('tbody')
          .find('tr')
          .should('have.length', 3)
      })
    })
  })
})
