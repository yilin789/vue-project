describe('Home page', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Shows a header', () => {
    cy.get('.vue-title')
      .should('contain', 'Welcome to reader club !!')
  })

  describe('Navigation bar', () => {
    it('Shows the required links', () => {
      cy.get('.navbar-nav')
        .eq(0)
        .within(() => {
          cy.get('.nav-item')
            .eq(0)
            .should('contain', 'Home')
          cy.get('.nav-item')
            .eq(1)
            .should('contain', 'Books')
          cy.get('.nav-item')
            .eq(2)
            .should('contain', 'Add a New Book')
        })
      cy.get('.navbar-nav')
        .eq(1)
        .within(() => {
          cy.get('.nav-item')
            .eq(0)
            .should('contain', 'Login')
          cy.get('.nav-item')
            .eq(1)
            .should('contain', 'New Reader')
        })
    })

    it('Redirects when links are clicked', () => {
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(1)
        .click()
      cy.url().should('contain', '/books')
      cy.get('.navbar-nav')
        .eq(0)
        .find('.nav-item')
        .eq(2)
        .click()
      cy.url().should('contain', '/addBook')
      cy.get('.navbar-nav')
        .eq(1)
        .find('.nav-item')
        .eq(0)
        .click()
      cy.url().should('contain', '/login')
      cy.get('.navbar-nav')
        .eq(1)
        .find('.nav-item')
        .eq(1)
        .click()
      cy.url().should('contain', '/addReader')
    })
  })

  describe('Link in page', () => {
    it('Redirects when clicked', () => {
      cy.get('[data-test=link]')
        .click()
      cy.url().should('contain', '/books')
    })
  })
})
