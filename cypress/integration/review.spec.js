const apiURL_reader = 'https://web-api-staging-ys.herokuapp.com/readers/'
const apiURL_book = 'https://web-api-staging-ys.herokuapp.com/books/'

var reader1 = {
  username: 'testName1',
  password: 'testPsd1'
}
var reader2 = {
  username: 'testName2',
  password: 'testPsd2'
}

describe('Pages about reviews', () => {
  before(() => {
    cy.request('POST', apiURL_reader, reader1)
    cy.request('POST', apiURL_reader, reader2)
    cy.request(apiURL_book)
      .its('body')
      .then(books => {
        books.forEach(e => {
          cy.request('DELETE', `${apiURL_book}${e._id}`)
        })
      })
    cy.fixture('books').then(books => {
      let [b1, b2, b3] = books
      let three = [b1, b2, b3]
      three.forEach(b => {
        cy.request('POST', apiURL_book, b)
      })
    })
    cy.visit('/#/login')
    cy.get('#username_lbl')
      .contains('Username')
      .next()
      .type('testName1')
    cy.get('#psd_lbl')
      .contains('Password')
      .next()
      .type('testPsd1')
    cy.get('#btn')
      .contains('Login')
      .click()
  })

  describe('Add new review page', () => {
    before(() => {
      cy.visit('/#/books')
      cy.wait(1000)
      cy.get('tbody')
        .find('tr')
        .eq(0)
        .find('td')
        .eq(7)
        .find('a')
        .click()
      cy.get('#addBtn')
        .click()
    })

    it('Shows a header', () => {
      cy.get('.vue-title')
        .should('contain', 'Add Your Review')
    })

    describe('With invalid/blank attributes', () => {
      it('Shows error messages', () => {
        cy.get('.error')
          .should('contain', 'is Required')
        cy.get('#content_input')
          .type('I')
        cy.get('.error')
          .should('contain', 'at least')
        cy.get('#btn_submit')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Please Fill in the Form Correctly.')
      })
    })

    describe('With valid attributes', () => {
      it('Adds a new review', () => {
        cy.get('#score')
          .select("5")
        cy.get('.form__label')
          .contains('Your Comment')
          .next()
          .type(' like this book!')
        cy.get('#btn_submit')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Thanks!')
        cy.get('#btn_return')
          .click()
        cy.get('tbody')
          .find("tr")
          .should("have.length", 1)
      })
    })
  })

  describe('Edit review page', () => {
    before(() => {
      cy.get('tbody')
        .find('tr')
        .eq(0)
        .find('td')
        .eq(6)
        .find('a')
        .click()
    })

    it('Shows a header', () => {
      cy.get('.vue-title')
        .should('contain', 'Change Your Review')
    })

    describe('With invalid/blank attributes', () => {
      it('Shows error messages', () => {
        cy.get('.error')
          .should('contain', 'is Required')
        cy.get('.form__input')
          .type('I')
        cy.get('.error')
          .should('contain', 'at least')
        cy.get('#btn_submit')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Please Fill in the Form Correctly.')
      })
    })

    describe('With valid attributes', () => {
      it('Changes a review', () => {
        cy.get('.form__label')
          .contains('New Comment')
          .next()
          .type(' love this book!')
        cy.get('#btn_submit')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Thanks!')
        cy.get('#btn_return')
          .click()
        cy.get('tbody')
          .find("tr")
          .should("have.length", 1)
      })
    })
  })

  describe('Reviews page', () => {
    beforeEach(() => {
      cy.visit('/#/books')
      cy.wait(1000)
      cy.get('tbody')
        .find('tr')
        .eq(0)
        .find('td')
        .eq(7)
        .find('a')
        .click()
    })

    describe('Like', () => {
      it('Increments the review\'s likes', () => {
        cy.wait(200)
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(5)
          .find('a')
          .click()
        cy.wait(200)
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(4)
          .should('contain', 1)
      })
    })

    describe('Try to modify/delete other\'s review', () => {
      before(() => {
        cy.visit('/#/login')
        cy.get('#username_lbl')
          .contains('Username')
          .next()
          .type('testName2')
        cy.get('#psd_lbl')
          .contains('Password')
          .next()
          .type('testPsd2')
        cy.get('#btn')
          .contains('Login')
          .click()
      })

      it('Cannot modify', () => {
        cy.wait(200)
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(6)
          .find('a')
          .click()
        cy.get('.swal2-content')
          .should('contain', 'You can\'t modify')
      })

      it('Cannot delete', () => {
        cy.wait(200)
        cy.get('tbody')
          .find('tr')
          .eq(0)
          .find('td')
          .eq(7)
          .find('a')
          .click()
        cy.get('.swal2-content')
          .should('contain', 'You can\'t delete')
      })
    })

    describe('Delete review', () => {
      before(() => {
        cy.visit('/#/login')
        cy.get('#username_lbl')
          .contains('Username')
          .next()
          .type('testName1')
        cy.get('#psd_lbl')
          .contains('Password')
          .next()
          .type('testPsd1')
        cy.get('#btn')
          .contains('Login')
          .click()
      })

      describe('For a cancelled delete operation', () => {
        it('Leaves the list unchanged', () => {
          cy.wait(200)
          cy.get('tbody')
            .find('tr')
            .eq(0)
            .find('td')
            .eq(7)
            .find('a')
            .click()
          cy.get('button')
            .contains('Cancel')
            .click()
          cy.get('tbody')
            .find('tr')
            .should('have.length', 1)
        })
      })

      describe('For a confirmed delete operation', () => {
        it('Reduces the no. of reviews by 1', () => {
          cy.wait(200)
          cy.get('tbody')
            .find('tr')
            .eq(0)
            .find('td')
            .eq(7)
            .find('a')
            .click()
          cy.get('button')
            .contains('Delete')
            .click()
          cy.get('tbody')
            .contains('tr')
            .should('not.exist')
        })
      })
    })
  })

  after(() => {
    var id = ''
    cy.wait(1000)
      .request('GET', `${apiURL_reader}testName1`)
      .its('body')
      .then(reader => {
        id = reader._id
        console.log(id)
        cy.request('DELETE', `${apiURL_reader}${id}`)
      })
    cy.wait(1000)
      .request('GET', `${apiURL_reader}testName2`)
      .its('body')
      .then(reader => {
        id = reader._id
        console.log(id)
        cy.request('DELETE', `${apiURL_reader}${id}`)
      })
  })
})
