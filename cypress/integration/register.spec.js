const apiURL = 'https://web-api-staging-ys.herokuapp.com/readers/'

describe('Register page', () => {
  beforeEach(() => {
    cy.visit('/#/addReader')
  })

  it('Shows a header', () => {
    cy.get('.vue-title')
      .should('contain', 'Welcome to join us')
  })

  describe('Add a reader', () => {
    describe('With valid attributes', () => {
      it('Allows reader to be submitted', () => {
        cy.get('#username')
          .contains('Username (Less than 20 Characters)')
          .next()
          .type('testName')
        cy.contains('Username is Required')
          .should('not.exist')
        cy.contains('Username is too Long')
          .should('not.exist')
        cy.get('#psd')
          .contains('Password')
          .next()
          .type('testPsd')
        cy.contains('Password is Required')
          .should('not.exist')
        cy.get('#psdConfirm')
          .contains('Please Confirm Your Password')
          .next()
          .type('testPsd')
        cy.contains('Passwords Entered Twice are Inconsistent')
          .should('not.exist')
        cy.get('.typo__p')
          .should('not.exist')
        cy.get('#btn')
          .contains('Register')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Welcome!')
      })

      after(() => {
        var id = ''
        cy.wait(1000)
          .request('GET', `${apiURL}testName`)
          .its('body')
          .then(reader => {
            id = reader._id
            console.log(id)
            cy.request('DELETE', `${apiURL}${id}`)
          })
      })
    })

    describe('With invalid/blank attributes', () => {
      it('Shows error messages', () => {
        cy.get('.error')
          .should('contain', 'is Required')
        cy.get('#username_input')
          .type('badName-badName-badName-badName')
        cy.get('.error')
          .contains('Username is too Long')
          .should('exist')
        cy.get('#psd_input')
          .type('right')
        cy.get('#psdConfirm_input')
          .type('wrong')
        cy.get('.error')
          .contains('Passwords Entered Twice are Inconsistent')
          .should('exist')
        cy.get('#btn')
          .click()
        cy.get('.typo__p')
          .should('contain', 'Please Fill in the Form Correctly.')
      })
    })
  })
})
