# Assignment 2 - Agile Software Practice.

Name: Shuo Yang

## Client UI.

![][home-page]

>> Home page

![][register]

>> Register page - add a new reader

![][login]

>> Login page

![][addBook]

>> Allows the user add a new book

![][books]

>> Allows the user delete books and view reviews

![][addReview]

>> Allows the user add a new review

![][editReview]

>> Allows the user edit review content

![][reviews]

>> Allows the user like and delete review (can't delete or edit other readers' reviews)

## E2E/Cypress testing.

https://dashboard.cypress.io/projects/2vkfun/runs

## Web API CI.

https://yilin789.gitlab.io/web-api-project/coverage/lcov-report

[home-page]: ./img/home-page.png

[register]: ./img/register.png

[login]: ./img/login.png

[addBook]: ./img/addBook.png

[books]: ./img/books.png

[addReview]: ./img/addReview.png

[editReview]: ./img/editReview.png

[reviews]: ./img/reviews.png
