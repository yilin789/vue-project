import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Books from '@/components/Books'
import AddBook from '@/components/AddBook'
import AddReader from '@/components/AddReader'
import Login from '@/components/Login'
import Reviews from '@/components/Reviews'
import AddReview from '@/components/AddReview'
import EditReview from '@/components/EditReview'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/books',
      name: 'Books',
      component: Books
    },
    {
      path: '/addBook',
      name: 'AddBook',
      component: AddBook
    },
    {
      path: '/addReader',
      name: 'AddReader',
      component: AddReader
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/reviews',
      name: 'Reviews',
      component: Reviews
    },
    {
      path: '/addReview',
      name: 'AddReview',
      component: AddReview
    },
    {
      path: '/editReview',
      name: 'EditReview',
      component: EditReview
    },
  ]
})
