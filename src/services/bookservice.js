import Api from '../services/api'

export default {
  fetchBooks() {
    return Api().get('/books')
  },
  postBook(book) {
    return Api().post('/books', book,
      {headers: {'Content-type': 'application/json'}})
  },
  deleteBook(id) {
    return Api().delete(`/books/${id}`)
  },
  postReader(reader) {
    return Api().post('/readers', reader,
      {headers: {'Content-type': 'application/json'}})
  },
  getReader(name) {
    return Api().get(`/readers/${name}`)
  },
  deleteReader(id) {
    return Api().delete(`/readers/${id}`)
  },
  fetchReviews(bookId) {
    return Api().get(`/reviews/${bookId}/book`)
  },
  deleteReview(id) {
    return Api().delete(`/reviews/${id}`)
  },
  like(id) {
    return Api().put(`/reviews/${id}/likes`)
  },
  postReview(review) {
    return Api().post('/reviews', review,
      {headers: {'Content-type': 'application/json'}})
  },
  putReview(id, review) {
    return Api().put(`/reviews/${id}/content`, review,
      {headers: {'Content-type': 'application/json'}})
  }
}
